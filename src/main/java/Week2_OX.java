/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author Mynx
 */
import java.util.Scanner;

public class Week2_OX {

    public static Scanner sc = new Scanner(System.in);
    static int row, col;
    static int count = 0;
    static boolean isFinish = false;
    static char winner = '-';
    static char player = 'X';
    static char[][] board = {
        {'-', '-', '-'},
        {'-', '-', '-'},
        {'-', '-', '-'}};

    static void showWelcome() {
        System.out.println("Welcome to OX Game");

    }

    static void showTable() {

        System.out.println();

        System.out.println(" 1 2 3");
        for (int row = 0; row < 3; row++) {
            System.out.print(row + 1);
            for (int col = 0; col < 3; col++) {
                System.out.print(board[row][col] + " ");

            }
            System.out.println("");
        }
    }

    static void showTurn() {
        System.out.println(player + " turn");
    }

    static void input() {
        while (true) {
            System.out.println("Please input Row col:");
            row = sc.nextInt() - 1;
            col = sc.nextInt() - 1;
            if (board[row][col] == '-') {
                board[row][col] = player;
                break;
            }
            System.out.println("Error table at row and col is not Empty!!!");
        }
    }

    static void checkCol() {

        for (int row = 0; row < 3; row++) {
            if (board[row][col] != player) {
                return;
            }
        }
        isFinish = true;
        winner = player;
    }

    static void checkRow() {

        for (int col = 0; col < 3; col++) {
            if (board[row][col] != player) {
                return;
            }

        }
        isFinish = true;
        winner = player;
    }

    static void checkWin() {
        checkRow();
        checkCol();

    }

    static void switchPlayer() {
        if (player == 'X') {
            player = 'O';
        } else {
            player = 'X';
        }
    }

    static void checkDraw() {
        if (count == 9) {
            isFinish = true;
        }
    }

    static void showResult() {
        if (winner == '-') {
            System.out.println("Draw");
        } else {
            System.out.println("Player " + winner + " Win....");
        }
    }

    static void showBye() {
        System.out.println("Bye Bye ....");
    }

    public static void main(String[] args) {
        int count = 1;
        showWelcome();
        try {
            do {
                showTable();
                showTurn();
                input();
                checkWin();
                switchPlayer();
            } while (!isFinish);
            showResult();
            showBye();
        } catch (ArrayIndexOutOfBoundsException e) {
            System.out.println("Index not found");
        }

    }
}
